import React from 'react';
import './App.css';
import OrderList from './components/OrderList';
import Navbar from './components/Navbar';
import List from './components/List';

function App() {
  return (
    <div className="App">
      <Navbar title="Sin esperas" />
      <OrderList />
    </div>
  );
}

export default App;
