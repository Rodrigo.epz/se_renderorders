import React from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.css';
import './styles/OrderList.css'

class OrderList extends React.Component {

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      collapse: true,
      data: [],
    };
  }

  componentDidMount() {
    const requestOptions = {
      method: 'GET',
      headers: this.simpleHeader()
    };

    const url = 'http://127.0.0.1:4000/orders';
    fetch(url, requestOptions).then(response => {

      return response.json();
    }).then(data => {
      this.setState({ data: data });
    })
  }

  simpleHeader() {
    return {
      'Content-Type': 'application/json',
      'mode': 'no-cors',
    };
  }

  toggle() {
    this.setState(state => ({ collapse: !state.collapse }));
  }

  sentState(e) {
    console.log("Id de la orden " + e.target.id);

    var url = 'https://example.com/profile';
    var id = e.target.id;

    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(id), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/json',
        'mode': 'no-cors'
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));
  }

  render() {

    return (
      <div>
        < div className="card" >
          <div className="card-header header">
            <div className="">
              <h1>
                <Button color="btn btn-link btn-order" onClick={this.toggle} >Order 1</Button>
              </h1>
            </div>
            <div className="">
              <h2>
                <Button color="btn btn-success btn-order" onClick={this.sentState} >Despachado</Button>
              </h2>
            </div>
          </div>
          <Collapse isOpen={this.state.collapse}>
            <Card>
              <CardBody>
                <div></div>
                <ul>
                  {this.state.data.map((item, index) => {
                    return Object.keys(item.food).map((type, key) => {

                      return item.food[type].map(el => {
                        // console.log(el);
                        console.log('hola mundo ' + JSON.stringify(item))
                        return <li className='list-style'>{el.name}</li>
                      })
                    })
                  })}

                </ul>
              </CardBody>
            </Card>
          </Collapse>
        </div >
      </div>
    )
  }
}

export default OrderList;