import React from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import socketIOClient from "socket.io-client";
import axios from 'axios';

import 'bootstrap/dist/css/bootstrap.css';
import './styles/OrderList.css'

class OrderList extends React.Component {

	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
		this.state = {
			collapse: true,
			data: [],
			endpoint: '//127.0.0.1:4000'
		};

		this.data = this.arrayData(this.array);
	}

	arrayData(arr) {

	};

	componentDidMount() {
		const requestOptions = {
			method: 'GET',
			headers: this.simpleHeader()
		};

		const url = 'http://127.0.0.1:4000/orders';
		fetch(url, requestOptions).then(response => {

			return response.json();
		}).then(data => {
			this.setState({ data: data });
		})

		const { endpoint } = this.state;
		const socket = socketIOClient(endpoint);
		console.log(this.state.data);
		socket.on("sendOrders", data => this.setState({ data: data }));
	}

	simpleHeader() {
		return {
			'Content-Type': 'application/json',
			'mode': 'no-cors',
		};
	}

	toggle() {
		this.setState(state => ({ collapse: !state.collapse }));
	}

	sentState(e) {
		console.log("Id de la orden " + e.target.id);

		var url = 'http://127.0.0.1:4000/orderDispatch';
		var id = e.target.id;

		axios.post(url, { id })
			.then(res => {
				console.log(res);
				console.log(res.data);
			})

		let order = 'order' + id;
		let div = document.getElementById(order);
		div.parentNode.removeChild(div);
	}

	render() {

		return (
			<div>
				{this.state.data.map((item, index) => {
					return (
						< div className="card-parent" id={'order' + item.id}>
							<div className="card-header header">
								<div className="">
									<h1>
										<Button color="btn btn-link btn-order" onClick={this.toggle} >Order {item.id}</Button>
									</h1>
								</div>
								<div className="">
									<h2>
										<Button color="btn btn-success btn-order" id={item.id} onClick={this.sentState} >Despachado</Button>
									</h2>
								</div>
							</div>
							<Collapse isOpen={this.state.collapse}>
								<Card>
									<CardBody>
										<table className="table">
											<tbody>
												<tr>
													{Object.keys(item.food).map((type, key) => {
														// let typeItem = type ? <th>{type}</th> : <th>N/A</th>
														return <th>{type}</th>

													})}
												</tr>
												<tr>
													{Object.keys(item.food).map((type, key) => {
														return <td>
															{
																item.food[type].map(el => {
																	console.log('id orden: ' + el.id);
																	let conItem = el ? <p className='list-style'>{el.name}</p> : <p>N/A</p>
																	return conItem;
																})
															}
														</td>
													})}
												</tr>
											</tbody>
										</table>
									</CardBody>
								</Card>
							</Collapse>
						</div >
					)
				})}
			</div>
		)
	}
}

export default OrderList;