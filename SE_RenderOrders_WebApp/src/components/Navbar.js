import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './styles/Navbar.css';
import Logo from './Logo sin esperas 2.png';

class Navbar extends React.Component {
  render() {

    return (
      <div className="Navbar">
        <div className="container-fluid">
          <h1>{this.props.title}</h1>
        </div>
      </div>
    )
  }
}

export default Navbar;